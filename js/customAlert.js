/**
 * Created by nagkumar on 09/11/15.
 */
window.onload = function()  {
    drawImage();
    window.existing_boxes = [];
};

var canvas;
var ctx;
var canvasOffset;
var offsetX;
var offsetY;
var isDrawing = false;
canvas = document.getElementById("canvas");
ctx = canvas.getContext("2d");
canvasOffset = $("#canvas").offset();
offsetX = canvasOffset.left;
offsetY = canvasOffset.top;
var startX;
var startY;
var new_rectangles = [];

//draws the boxes after saving one box
function drawBoxes(){
    var i;
    for(i=0;i<existing_boxes.length;i++)    {
        var x1 = existing_boxes[i]['x1'];
        var y1 = existing_boxes[i]['y1'];
        var w = existing_boxes[i]['x2'];
        var h = existing_boxes[i]['y2'];
        var s = existing_boxes[i]['inputString'];
        ctx.beginPath();
        console.log(x1 + " " + y1 + " " + w + " " + h);
        ctx.rect(x1, y1, w, h);
        ctx.lineWidth = 10;
        ctx.strokeStyle='#cc0000';
        ctx.stroke();
        ctx.font = "48px serif";
        ctx.fillStyle="#FF0000";
        ctx.fillText(s, (x1 + (w/4)), (y1 + (h/2)));
    }
}

//draws the image on canvas
function drawImage()    {
    var c = document.getElementById("canvas");
    var ctx = c.getContext("2d");
    var img = document.getElementById("image");
    ctx.drawImage(img, 10, 10, c.width, c.height);
}

//binding event handlers using jquery
$("#canvas").on('mousedown', function (e) {
    handleMouseDown(e);
}).on('mouseup', function(e) {
    handleMouseUp(e);
}).on('mousemove', function(e) {
    handleMouseMove(e);
});

//function to handle end of click. This calls a function "callCustomAlert" which displays the sweetalert
function handleMouseUp(e) {
    isDrawing = false;
    canvas.style.cursor = "default";
    callCustomAlert(new_rectangles[0],  new_rectangles[1], new_rectangles[2], new_rectangles[3]);
    new_rectangles.pop();new_rectangles.pop();new_rectangles.pop();new_rectangles.pop();
}

//clears all the rectangles. Used when user is selecting the rectangles and the changes have to be reflected on canvas
function clearRectangles()  {
    ctx.clearRect(new_rectangles[0], new_rectangles[1], new_rectangles[2], new_rectangles[3]);
    new_rectangles.pop();new_rectangles.pop();new_rectangles.pop();new_rectangles.pop();
}

//handles the event when user moves the cursor after he starts selecting. This function calls the clear function and
//draws a new rectangle with the user selected coordinates
function handleMouseMove(e) {
    if (isDrawing) {
        var mouseX = parseInt(e.clientX - offsetX);
        var mouseY = parseInt(e.clientY - offsetY);

        clearRectangles();
        drawImage();
        ctx.beginPath();
        ctx.rect(startX, startY + window.scrollY, mouseX - startX, mouseY - startY);
        new_rectangles.push(startX, startY + window.scrollY, mouseX - startX, mouseY - startY);
        ctx.lineWidth = 10;
        ctx.strokeStyle='#cc0000';
        ctx.stroke();

    }
}


//event hadler that handles the event when user starts the selection
function handleMouseDown(e) {
    canvas.style.cursor = "crosshair";
    isDrawing = true;
    startX = parseInt(e.clientX - offsetX);
    startY = parseInt(e.clientY - offsetY);

}


//function to send data to parse. This function calls redraw to draw all the boxes which were created in the previous attempt
function sendToParse(UPC, x1, y1, x2, y2, inputStr)  {
    Parse.initialize("4P9kMuhbnAfeyjhWtAHL7xeseDV0ABIoZwt2EVJz", "nrJdQb1tBz93kgv4kzTPxTQV3ylZS7HEsnorAajs");
    var ImageIdentifier = Parse.Object.extend("ImageIdentifier");
        var obj = new ImageIdentifier();
            obj.save({
              UPC: UPC,
              x1: x1,
              y1: y1,
              w: x2,
              h: y2,
              inputString: inputStr
          });
    box = {
        x1: x1,
        y1: y1,
        x2: x2,
        y2: y2,
        inputString: inputStr
    };
    existing_boxes.push(box);
    drawImage();
    drawBoxes();

}

//function that displays the sweet alert and gets a confirmation. This also populates the select2 field
// which searches for the string
function callCustomAlert(x1, y1, x2, y2) {
    swal({
            title: "Name the product",
            html:
                '<div style="height: 300px"><select name="objects" id="objects" style="z-index: 9999"><option></option></select></div>',
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm) {
            var val = $("#objects").val();
            if(isConfirm)   {
                if(val.indexOf("##") != -1) {
                    var parts = val.split("##");
                    sendToParse(parts[0], x1, y1, x2, y2, parts[1]);
                    swal(
                        'Added!',
                        'Your item has been added.',
                        'success'
                    );
                }
                else    {
                    swal({
                        html:'You entered: <strong>' + $('#objects').val() + '</strong>. <p>Are you sure to save the new item?</p>',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(isConfirm){
                        if(isConfirm) {
                            sendToParse(null, x1, y1, x2, y2, val);
                        }
                    }

                    );

                }
            }
            else    {
                drawImage();
                drawBoxes();
            }
        }
    );
    var items = [];
    $.getJSON("/data/draegersDatabaseSheet.json", function(data){
        data = data['results'];
        $.each(data, function(key, val) {
            if(val['UPC'])  {
                var text = "";
                var k = val['UPC'];
                if(val["col7"]) {
                    text = text + val["col7"] + ", ";
                }
                if(val['col6']) {
                    text = text + val["col6"];
                    k = k + '##' + val['col6'];
                }
                else    {
                    text = val["Name"];
                    k = k + "##" + text;
                }
                items.push({
                    "id": k,
                    "text": text
                })
            }
        });
        $("#objects").select2({
            data:items,
            tags: true,
            maximumSelectionSize: 1,
            placeholder: "Search for product",
            allowClear: true,
            maximumSelectionLength: 5
        });
    });
}